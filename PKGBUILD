# vim:set ft=sh:
# Maintainer: BlackEagle < ike DOT devolder AT gmail DOT com >
# Contributor: Bartłomiej Piotrowski <barthalion@gmail.com>
# Contributor: Mateusz Herych <heniekk@gmail.com>
# Contributor: ruario <ruario AT opera DOT com>
# Contributor: Daniel Isenmann <daniel AT archlinux DOT org>
# Contributor: dorphell <dorphell AT archlinux DOT org>
# Contributor: Sigitas Mazaliauskas <sigis AT gmail DOT com>
# Contributor: eworm
# Modification of opera.desktop so that it starts in Spanish from Rafael from the azul repository
# Codec added (Rafael from azul repository)

pkgname=opera-spanish
fname=opera
pkgver=100.0.4815.47
pkgrel=1
pkgdesc="A fast and secure web browser. Also install extra codecs."
codec_file=0.77.0
url="https://www.opera.com/"
codec_url="https://github.com/nwjs-ffmpeg-prebuilt/nwjs-ffmpeg-prebuilt"
options=(!strip !zipman)
license=('custom:opera')
backup=("etc/${fname}/default")
arch=('x86_64')
depends=('gtk3' 'alsa-lib' 'libnotify' 'curl' 'nss' 'libcups' 'libxss' 'ttf-font' 'desktop-file-utils' 'shared-mime-info' 'hicolor-icon-theme')
optdepends=(
    'upower: opera battery save'
)

conflicts=("opera" "opera-ffmpeg-codecs")

source=(
    "https://get.geo.opera.com/pub/${fname}/desktop/${pkgver}/linux/${fname}-stable_${pkgver}_amd64.deb"
    "opera"
    "default"
    'eula.html'
    'terms.html'
    'privacy.html'
    'opera.desktop'
    "${codec_url}/releases/download/${codec_file}/${codec_file}-linux-x64.zip"
)
sha512sums=('ca48bf5aa8a100551c783de14778727583113d8c1325b7e65ba70a84b49a0e50da90848509429b5deafadb74399a0ad62087c785c1cc29bef0401cc3c0071d5b'
            '7e854e4c972785b8941f60117fbe4b88baeb8d7ca845ef2e10e8064043411da73821ba1ab0068df61e902f242a3ce355b51ffa9eab5397ff3ae3b5defd1be496'
            'ddb1773877fcfd7d9674e63263a80f9dd5a3ba414cda4cc6c411c88d49c1d5175eede66d9362558ddd53c928c723101e4e110479ae88b8aec4d2366ec179297f'
            'aaaa4435a3b6a08bf8e6ad4802afcbf111c1e8f477054251f031b70ae57ac1234fa19048121d64c878dc3b1de03522ce7ef11a263a86dc7062f643d569ecff82'
            '800d62321344ff4e3521ff20fae281cad9206bae80e60965784d144f8bf852f756cbc21f4c9d8d4e93d026da7ca10e0eda7601c83a6d8d85125831eacb907d9a'
            '43d4a066758805597527dbdfc95b4c8ad4b22c5db812b9493e50f8820c72f30c1e431bed40fdb821ab0c23a63aa31dc0e946ab708cc23ac617446964fa6b96f2'
            '848f754e89c0f960b7aacc1d949599981a6daf3eda4b1f0db1df30172626ac1bdbacbad504eaf207929ebd1ce6d8a46b4e10e5573572f1bd0591f2b65141ea9a'
            'fe449eebc7dfc14889b44c0678fef7bec888ffb8c3675465db9eb2f65ce6476746dbfc394c53b06848e6774bc0cd445daa81301607e2d17f33e099d807c71e99')

prepare() {
    sed -e "s/%fname%/${fname}/g" -i "${srcdir}/opera"
    sed -e "s/%operabin%/${fname}\/${fname}/g" \
        -i "${srcdir}/opera"

}

package() {
    tar -xf data.tar.xz --exclude=usr/share/{lintian,menu} -C "${pkgdir}/"

    # get rid of the extra subfolder {i386,x86_64}-linux-gnu
    (
        cd "${pkgdir}/usr/lib/"*-linux-gnu/
        mv "${fname}" ../
    )
    rm -rf "${pkgdir}/usr/lib/"*-linux-gnu

    # suid opera_sandbox
    chmod 4755 "${pkgdir}/usr/lib/${fname}/opera_sandbox"

    # install default options
    install -Dm644 "${srcdir}/default" "${pkgdir}/etc/${fname}/default"

    # install opera wrapper
    rm "${pkgdir}/usr/bin/${fname}"
    install -Dm755 "${srcdir}/opera" "${pkgdir}/usr/bin/${fname}"

    # license
    install -Dm644 \
        "${pkgdir}/usr/share/doc/${fname}-stable/copyright" \
        "$pkgdir/usr/share/licenses/${fname}/copyright"

    # eula
    install -Dm644 \
        "${srcdir}/eula.html" \
        "${pkgdir}/usr/share/licenses/${fname}/eula.html"

    # terms
    install -Dm644 \
        "${srcdir}/terms.html" \
        "${pkgdir}/usr/share/licenses/${fname}/terms.html"

    # privacy
    install -Dm644 \
        "${srcdir}/privacy.html" \
        "${pkgdir}/usr/share/licenses/${fname}/privacy.html"

    # opera.desktop in spanish
    install -d ${pkgdir}/usr/share/applications/
    install -Dm644 opera.desktop ${pkgdir}/usr/share/applications/
    
    # Add codec file
    install -Dm644 ${srcdir}/libffmpeg.so \
    "${pkgdir}/usr/lib/opera/lib_extra/libffmpeg.so"
}

