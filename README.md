# opera-spanish

**Modification of opera.desktop so that it starts in Spanish.**

Without using chroot it compiles fine.

A fast and secure web browser, spanish version. Also install extra codecs: **opera-ffmpeg-codecs**.
If you have opera installed from the Arch Linux repository, you will need to uninstall it to install opera-spanish.

https://get.geo.opera.com/pub/opera/desktop/

<br><br>

How to clone this repository:
```
git clone https://gitlab.com/azul4/content/web-browsers/opera-spanish.git
```



